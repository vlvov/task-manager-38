package ru.t1.vlvov.tm.exception.system;

public class ServiceNotFoundException extends AbstractSystemException {

    public ServiceNotFoundException() {
        super("Error! Service not defined...");
    }

}