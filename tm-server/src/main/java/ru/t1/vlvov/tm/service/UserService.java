package ru.t1.vlvov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.api.repository.IUserRepository;
import ru.t1.vlvov.tm.api.service.*;
import ru.t1.vlvov.tm.enumerated.Role;
import ru.t1.vlvov.tm.exception.entity.UserNotFoundException;
import ru.t1.vlvov.tm.exception.field.*;
import ru.t1.vlvov.tm.exception.user.ExistsEmailException;
import ru.t1.vlvov.tm.exception.user.ExistsLoginException;
import ru.t1.vlvov.tm.model.User;
import ru.t1.vlvov.tm.repository.UserRepository;

import java.sql.Connection;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final IPropertyService propertyService;

    public UserService(
            @NotNull final IConnectionService connectionService,
            @NotNull final IProjectService projectService,
            @NotNull final ITaskService taskService,
            @NotNull final IPropertyService propertyService
    ) {
        super(connectionService);
        this.projectService = projectService;
        this.taskService = taskService;
        this.propertyService = propertyService;
    }

    @NotNull
    public IUserRepository getRepository(@NotNull final Connection connection) {
        return new UserRepository(propertyService, connection);
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            user = repository.create(login, password);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (isEmailExist(email)) throw new ExistsEmailException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            user = repository.create(login, password, email);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @NotNull final User user;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            user = repository.create(login, password, role);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            user = repository.findByLogin(login);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final User user;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            user = repository.findByEmail(email);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @NotNull
    @Override
    public User removeByLogin(@Nullable final String login) {
        @Nullable final User user = findByLogin(login);
        remove(user);
        return user;
    }

    @NotNull
    @Override
    public User removeByEmail(@Nullable final String email) {
        @Nullable final User user = findByEmail(email);
        remove(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            user = repository.setPassword(id, password);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            user = repository.updateUser(id, firstName, lastName, middleName);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @Override
    @SneakyThrows
    public boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        boolean isExist = false;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            isExist = repository.isLoginExist(login);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return isExist;
    }

    @Override
    @SneakyThrows
    public boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        boolean isExist = false;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            isExist = repository.isEmailExist(email);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return isExist;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            user = repository.lockUser(user.getId());
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            user = repository.unlockUser(user.getId());
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User remove(@Nullable final User model) {
        if (model == null) throw new UserNotFoundException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            repository.remove(model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return model;
    }

}
