package ru.t1.vlvov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.api.repository.ISessionRepository;
import ru.t1.vlvov.tm.constant.FieldConstant;
import ru.t1.vlvov.tm.enumerated.Role;
import ru.t1.vlvov.tm.model.Session;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull final Connection connection) {
        super(connection, "SESSION");
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session add(@NotNull Session model) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (ID, CREATED, ROLE, USER_ID)"
                        + "VALUES (?, ?, ?, ?)", getTable()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setTimestamp(2, new Timestamp(model.getDate().getTime()));
            statement.setString(3, model.getRole().getDisplayName());
            statement.setString(4, model.getUserId());
            statement.executeUpdate();
        }
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session add(@NotNull String userId, @NotNull Session model) {
        model.setUserId(userId);
        return add(model);
    }

    @NotNull
    @Override
    @SneakyThrows
    protected Session fetch(@NotNull final ResultSet rowSet) {
        @NotNull final Session session = new Session();
        session.setId(rowSet.getString(FieldConstant.ID));
        session.setUserId(rowSet.getString(FieldConstant.USER_ID));
        session.setDate(rowSet.getTimestamp(FieldConstant.CREATED));
        session.setRole(Role.toRole(rowSet.getString(FieldConstant.ROLE)));
        return session;
    }

}
