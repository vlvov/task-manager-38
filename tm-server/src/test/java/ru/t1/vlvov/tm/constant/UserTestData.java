package ru.t1.vlvov.tm.constant;

import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.model.User;

import java.util.Arrays;
import java.util.List;

public final class UserTestData {

    @NotNull
    public final static User USER1 = new User();

    @NotNull
    public final static User USER2 = new User();

    @NotNull
    public final static User USER3 = new User();

    @NotNull
    public final static User ADMIN1 = new User();

    @NotNull
    public final static List<User> USER_LIST = Arrays.asList(USER1, USER2, ADMIN1);

    @NotNull
    public final static List<User> USER_LIST2 = Arrays.asList(USER3, ADMIN1);

}
