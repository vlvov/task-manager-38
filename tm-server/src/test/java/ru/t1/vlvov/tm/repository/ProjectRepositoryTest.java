package ru.t1.vlvov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.vlvov.tm.api.repository.IProjectRepository;
import ru.t1.vlvov.tm.api.service.IConnectionService;
import ru.t1.vlvov.tm.api.service.IPropertyService;
import ru.t1.vlvov.tm.marker.UnitCategory;
import ru.t1.vlvov.tm.service.ConnectionService;
import ru.t1.vlvov.tm.service.PropertyService;

import java.sql.Connection;

import static ru.t1.vlvov.tm.constant.ProjectTestData.*;
import static ru.t1.vlvov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class ProjectRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @After
    @SneakyThrows
    public void tearDown() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            repository.clear(USER1.getId());
            repository.clear(USER2.getId());
            repository.clear(ADMIN1.getId());
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    public IProjectRepository getRepository(@NotNull final Connection connection) {
        return new ProjectRepository(connection);
    }

    @NotNull
    public Connection getConnection() {
        return connectionService.getConnection();
    }

    @Test
    @SneakyThrows
    public void add() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository projectRepository = getRepository(connection);
            Assert.assertFalse(projectRepository.existsById(USER1_PROJECT1.getId()));
            projectRepository.add(USER1_PROJECT1);
            connection.commit();
            Assert.assertEquals(USER1_PROJECT1.getId(), projectRepository.findOneById(USER1_PROJECT1.getId()).getId());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void addByUserId() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository projectRepository = getRepository(connection);
            projectRepository.add(USER1.getId(), USER1_PROJECT1);
            connection.commit();
            Assert.assertEquals(USER1.getId(), projectRepository.findAll().get(0).getUserId());
            Assert.assertEquals(USER1_PROJECT1.getId(), projectRepository.findAll().get(0).getId());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void clearByUserId() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository projectRepository = getRepository(connection);
            projectRepository.add(USER1_PROJECT1);
            projectRepository.add(USER2_PROJECT1);
            connection.commit();
            projectRepository.clear(USER1.getId());
            connection.commit();
            Assert.assertNull(projectRepository.findOneById(USER1_PROJECT1.getId()));
            Assert.assertEquals(USER2_PROJECT1.getId(), projectRepository.findOneById(USER2_PROJECT1.getId()).getId());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void findAllByUserId() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository projectRepository = getRepository(connection);
            projectRepository.add(PROJECT_LIST);
            connection.commit();
            Assert.assertFalse(projectRepository.findAll(USER1.getId()).isEmpty());
            projectRepository.clear(USER1.getId());
            connection.commit();
            Assert.assertTrue(projectRepository.findAll(USER1.getId()).isEmpty());
            Assert.assertFalse(projectRepository.findAll().isEmpty());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void findOneByIdByUserId() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository projectRepository = getRepository(connection);
            projectRepository.add(USER1_PROJECT1);
            projectRepository.add(USER2_PROJECT1);
            connection.commit();
            Assert.assertEquals(USER1_PROJECT1.getId(), projectRepository.findOneById(USER1.getId(), USER1_PROJECT1.getId()).getId());
            Assert.assertNull(projectRepository.findOneById(USER1.getId(), USER2_PROJECT1.getId()));
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void removeByUserId() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository projectRepository = getRepository(connection);
            projectRepository.add(USER1_PROJECT1);
            projectRepository.add(USER2_PROJECT1);
            connection.commit();
            projectRepository.remove(USER1.getId(), USER1_PROJECT1);
            connection.commit();
            Assert.assertNull(projectRepository.findOneById(USER1_PROJECT1.getId()));
            Assert.assertEquals(USER2_PROJECT1.getId(), projectRepository.findOneById(USER2_PROJECT1.getId()).getId());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void removeByIdByUserId() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository projectRepository = getRepository(connection);
            projectRepository.add(USER1_PROJECT1);
            projectRepository.add(USER2_PROJECT1);
            connection.commit();
            projectRepository.removeById(USER1.getId(), USER1_PROJECT1.getId());
            connection.commit();
            Assert.assertNull(projectRepository.findOneById(USER1_PROJECT1.getId()));
            Assert.assertEquals(USER2_PROJECT1.getId(), projectRepository.findOneById(USER2_PROJECT1.getId()).getId());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void existsByIdByUserId() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository projectRepository = getRepository(connection);
            projectRepository.add(USER1_PROJECT1);
            connection.commit();
            Assert.assertTrue(projectRepository.existsById(USER1_PROJECT1.getId()));
            Assert.assertFalse(projectRepository.existsById(USER2_PROJECT1.getId()));
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
